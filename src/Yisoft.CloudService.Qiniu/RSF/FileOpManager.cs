﻿//      )                             *     
//   ( /(        *   )       (      (  `    
//   )\()) (   ` )  /( (     )\     )\))(   
//  ((_)\  )\   ( )(_)))\ ((((_)(  ((_)()\  
// __ ((_)((_) (_(_())((_) )\ _ )\ (_()((_) 
// \ \ / / (_) |_   _|| __|(_)_\(_)|  \/  | 
//  \ V /  | | _ | |  | _|  / _ \  | |\/| | 
//   |_|   |_|(_)|_|  |___|/_/ \_\ |_|  |_| 
// 
// This file is subject to the terms and conditions defined in
// file 'License.txt', which is part of this source code package.
// 
// Copyright © Yi.TEAM. All rights reserved.
// -------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Net.Http;
using Newtonsoft.Json;
using Yisoft.CloudService.Qiniu.Common;
using Yisoft.CloudService.Qiniu.Http;
using Yisoft.CloudService.Qiniu.RSF.Model;

namespace Yisoft.CloudService.Qiniu.RSF
{
	/// <summary>
	/// 数据处理
	/// </summary>
	public class FileOpManager
	{
		private readonly HttpClient _client;
		private readonly Signature _signature;

		public FileOpManager(Mac mac)
		{
			_signature = new Signature(mac);
			_client = new HttpClient();
		}

		/// <summary>
		/// 生成管理凭证
		/// </summary>
		/// <param name="pfopUrl"></param>
		/// <param name="body"></param>
		/// <returns></returns>
		public string CreateManageToken(string pfopUrl, byte[] body) { return string.Format("QBox {0}", _signature.SignRequest(pfopUrl, body)); }

		/// <summary>
		/// 数据处理
		/// </summary>
		/// <param name="bucket">空间</param>
		/// <param name="key">空间文件的key</param>
		/// <param name="fops">操作(命令参数)</param>
		/// <param name="pipeline">私有队列</param>
		/// <param name="notifyUrl">通知url</param>
		/// <param name="force">forece参数</param>
		/// <returns></returns>
		public HttpResult Pfop(string bucket, string key, string fops, string pipeline, string notifyUrl, bool force)
		{
			var result = new HttpResult();
			var pfopUrl = Config.Zone.ApiHost + "/pfop/";

			var pfopParams = new Dictionary<string, string>
			{
				{"bucket", bucket},
				{"key", key},
				{"fops", fops}
			};

			if (!string.IsNullOrEmpty(notifyUrl)) pfopParams.Add("notifyURL", notifyUrl);
			if (force) pfopParams.Add("force", "1");
			if (!string.IsNullOrEmpty(pipeline)) pfopParams.Add("pipeline", pipeline);

			try
			{
				var req = new HttpRequestMessage(HttpMethod.Post, pfopUrl) {Content = new FormUrlEncodedContent(pfopParams)};
				var rs = req.Content.ReadAsByteArrayAsync();

				var token = CreateManageToken(pfopUrl, rs.Result);
				req.Headers.Add("Authorization", token);

				var msg = _client.SendAsync(req);
				result.StatusCode = (int) msg.Result.StatusCode;

				var ret = msg.Result.Content.ReadAsStringAsync();
				var pi = JsonConvert.DeserializeObject<PersistentInfo>(ret.Result);

				result.Message = pi.PersistentId;
			}
			catch (Exception ex)
			{
				result.Message = "[pfop] Error: " + ex.Message;
			}

			return result;
		}

		/// <summary>
		/// 数据处理
		/// </summary>
		/// <param name="bucket">空间</param>
		/// <param name="key">空间文件的key</param>
		/// <param name="fops">操作(命令参数)</param>
		/// <param name="pipeline">私有队列</param>
		/// <param name="notifyUrl">通知url</param>
		/// <param name="force">forece参数</param>
		/// <returns></returns>
		public HttpResult Pfop(string bucket, string key, string[] fops, string pipeline, string notifyUrl, bool force)
		{
			var newFops = string.Join(";", fops);

			return Pfop(bucket, key, newFops, pipeline, notifyUrl, force);
		}

		/// <summary>
		/// 查询pfop操作处理结果(或状态)
		/// </summary>
		/// <param name="persistentId">持久化ID</param>
		/// <returns></returns>
		public HttpResult Prefop(string persistentId)
		{
			var result = new HttpResult();
			var url = string.Format("{0}/status/get/prefop?id={1}", Config.Zone.ApiHost, persistentId);

			try
			{
				var msg = _client.GetAsync(url);
				result.StatusCode = (int) msg.Result.StatusCode;

				var ret = msg.Result.Content.ReadAsStringAsync();
				result.Message = ret.Result;
			}
			catch (Exception ex)
			{
				result.Message = "[prefop] Error: " + ex.Message;
			}

			return result;
		}
	}
}
