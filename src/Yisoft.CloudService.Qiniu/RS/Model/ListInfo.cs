﻿//      )                             *     
//   ( /(        *   )       (      (  `    
//   )\()) (   ` )  /( (     )\     )\))(   
//  ((_)\  )\   ( )(_)))\ ((((_)(  ((_)()\  
// __ ((_)((_) (_(_())((_) )\ _ )\ (_()((_) 
// \ \ / / (_) |_   _|| __|(_)_\(_)|  \/  | 
//  \ V /  | | _ | |  | _|  / _ \  | |\/| | 
//   |_|   |_|(_)|_|  |___|/_/ \_\ |_|  |_| 
// 
// This file is subject to the terms and conditions defined in
// file 'License.txt', which is part of this source code package.
// 
// Copyright © Yi.TEAM. All rights reserved.
// -------------------------------------------------------------------------------

using System.Collections.Generic;

namespace Yisoft.CloudService.Qiniu.RS.Model
{
	/// <summary>
	/// 获取空间文件(list操作)
	/// 
	/// 返回JSON字符串
	/// 
	/// {
	///     "marker":"MARKER",
	///     "items":
	///     [
	///         {
	///             "key":"KEY",
	///             "hash":"HASH",
	///             "fsize":FSIZE,
	///             "mimeType":"MIME_TYPE",
	///             "putTime":PUT_TIME
	///         },
	///         {
	///             ...
	///         }
	///     ],
	///     "CmmonPrefixes":"COMMON_PREFIXES"
	/// }
	/// 
	/// </summary>
	public class ListInfo
	{
		public string Marker { get; set; }

		public List<FileDesc> Items { get; set; }

		public List<string> CommonPrefixes { get; set; }
	}
}
