﻿//      )                             *     
//   ( /(        *   )       (      (  `    
//   )\()) (   ` )  /( (     )\     )\))(   
//  ((_)\  )\   ( )(_)))\ ((((_)(  ((_)()\  
// __ ((_)((_) (_(_())((_) )\ _ )\ (_()((_) 
// \ \ / / (_) |_   _|| __|(_)_\(_)|  \/  | 
//  \ V /  | | _ | |  | _|  / _ \  | |\/| | 
//   |_|   |_|(_)|_|  |___|/_/ \_\ |_|  |_| 
// 
// This file is subject to the terms and conditions defined in
// file 'License.txt', which is part of this source code package.
// 
// Copyright © Yi.TEAM. All rights reserved.
// -------------------------------------------------------------------------------

namespace Yisoft.CloudService.Qiniu.RS.Model
{
	/// <summary>
	/// 文件描述(stat操作返回消息中包含的有效内容)
	/// </summary>
	public class FileDesc
	{
		public string Key { get; set; }

		public string Hash { get; set; }

		public long Fsize { get; set; }

		public string MimeType { get; set; }

		public long PutTime { get; set; }
	}
}
