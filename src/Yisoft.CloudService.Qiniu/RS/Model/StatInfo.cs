﻿//      )                             *     
//   ( /(        *   )       (      (  `    
//   )\()) (   ` )  /( (     )\     )\))(   
//  ((_)\  )\   ( )(_)))\ ((((_)(  ((_)()\  
// __ ((_)((_) (_(_())((_) )\ _ )\ (_()((_) 
// \ \ / / (_) |_   _|| __|(_)_\(_)|  \/  | 
//  \ V /  | | _ | |  | _|  / _ \  | |\/| | 
//   |_|   |_|(_)|_|  |___|/_/ \_\ |_|  |_| 
// 
// This file is subject to the terms and conditions defined in
// file 'License.txt', which is part of this source code package.
// 
// Copyright © Yi.TEAM. All rights reserved.
// -------------------------------------------------------------------------------

using Newtonsoft.Json;

namespace Yisoft.CloudService.Qiniu.RS.Model
{
	/// <summary>
	/// 获取空间文件信息(stat操作)的有效内容
	/// </summary>
	[JsonObject(MemberSerialization.OptIn)]
	public class StatInfo
	{
		[JsonProperty("fsize")]
		public long Fsize { set; get; }

		[JsonProperty("hash")]
		public string Hash { set; get; }

		[JsonProperty("mimeType")]
		public string MimeType { set; get; }

		[JsonProperty("putTime")]
		public long PutTime { set; get; }
	}
}
