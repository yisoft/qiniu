﻿//      )                             *     
//   ( /(        *   )       (      (  `    
//   )\()) (   ` )  /( (     )\     )\))(   
//  ((_)\  )\   ( )(_)))\ ((((_)(  ((_)()\  
// __ ((_)((_) (_(_())((_) )\ _ )\ (_()((_) 
// \ \ / / (_) |_   _|| __|(_)_\(_)|  \/  | 
//  \ V /  | | _ | |  | _|  / _ \  | |\/| | 
//   |_|   |_|(_)|_|  |___|/_/ \_\ |_|  |_| 
// 
// This file is subject to the terms and conditions defined in
// file 'License.txt', which is part of this source code package.
// 
// Copyright © Yi.TEAM. All rights reserved.
// -------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using Newtonsoft.Json;
using Yisoft.CloudService.Qiniu.Common;
using Yisoft.CloudService.Qiniu.Http;
using Yisoft.CloudService.Qiniu.RS.Model;
using Yisoft.CloudService.Qiniu.Util;

namespace Yisoft.CloudService.Qiniu.RS
{
	/// <summary>
	/// 空间(资源)管理/操作
	/// </summary>
	public class BucketManager
	{
		private readonly HttpClient _client;
		private readonly Signature _signature;

		public BucketManager(Mac mac)
		{
			_signature = new Signature(mac);
			_client = new HttpClient();
		}

		/// <summary>
		/// 生成管理凭证
		/// </summary>
		/// <param name="url"></param>
		/// <returns></returns>
		public string CreateManageToken(string url)
		{
			//return string.Format("QBox {0}", signature.Sign(url));
			return CreateManageToken(url, null);
		}

		/// <summary>
		/// 生成管理凭证
		/// </summary>
		/// <param name="url"></param>
		/// <param name="body"></param>
		/// <returns></returns>
		public string CreateManageToken(string url, byte[] body) { return string.Format("QBox {0}", _signature.SignRequest(url, body)); }

		/// <summary>
		/// 获取空间文件信息
		/// </summary>
		/// <param name="bucket">空间名称</param>
		/// <param name="key">文件名称</param>
		/// <returns></returns>
		public StatResult Stat(string bucket, string key)
		{
			var result = new StatResult();
			var statUrl = Config.Zone.RsHost + StatOp(bucket, key);
			var token = CreateManageToken(statUrl);

			var req = new HttpRequestMessage(HttpMethod.Get, statUrl);
			req.Headers.Add("Authorization", token);

			try
			{
				var msg = _client.SendAsync(req);

				result.StatusCode = (int) msg.Result.StatusCode;

				var ret = msg.Result.Content.ReadAsStringAsync();

				result.StatInfo = JsonConvert.DeserializeObject<StatInfo>(ret.Result);
			}
			catch (Exception ex)
			{
				result.Message = "[stat] Error: " + ex.Message;
			}

			return result;
		}

		/// <summary>
		/// 获取空间(bucket)列表
		/// </summary>
		/// <returns></returns>
		public BucketsResult Buckets()
		{
			var result = new BucketsResult();
			var bucketsUrl = Config.Zone.RsHost + "/buckets";
			var token = CreateManageToken(bucketsUrl);

			var req = new HttpRequestMessage(HttpMethod.Get, bucketsUrl);
			req.Headers.Add("Authorization", token);

			try
			{
				var msg = _client.SendAsync(req);

				result.StatusCode = (int) msg.Result.StatusCode;

				var ret = msg.Result.Content.ReadAsStringAsync();

				result.Buckets = JsonConvert.DeserializeObject<List<string>>(ret.Result);
			}
			catch (Exception ex)
			{
				result.Message = "[buckets] Error: " + ex.Message;
			}

			return result;
		}

		/// <summary>
		/// 删除文件
		/// </summary>
		/// <param name="bucket"></param>
		/// <param name="key"></param>
		/// <returns></returns>
		public HttpResult Delete(string bucket, string key)
		{
			var result = new HttpResult();
			var deleteUrl = Config.Zone.RsHost + DeleteOp(bucket, key);
			var token = CreateManageToken(deleteUrl);

			var req = new HttpRequestMessage(HttpMethod.Post, deleteUrl);
			req.Headers.Add("Authorization", token);

			try
			{
				var msg = _client.SendAsync(req);

				result.StatusCode = (int) msg.Result.StatusCode;

				var ret = msg.Result.Content.ReadAsStringAsync();

				result.Message = ret.Result;
			}
			catch (Exception ex)
			{
				result.Message = "[delete] Error: " + ex.Message;
			}

			return result;
		}

		/// <summary>
		/// 复制文件
		/// </summary>
		/// <param name="srcBucket"></param>
		/// <param name="srcKey"></param>
		/// <param name="dstBucket"></param>
		/// <param name="dstKey"></param>
		/// <returns></returns>
		public HttpResult Copy(string srcBucket, string srcKey, string dstBucket, string dstKey)
		{
			var result = new HttpResult();
			var copyUrl = Config.Zone.RsHost + CopyOp(srcBucket, srcKey, dstBucket, dstKey);
			var token = CreateManageToken(copyUrl);

			var req = new HttpRequestMessage(HttpMethod.Post, copyUrl);
			req.Headers.Add("Authorization", token);

			try
			{
				var msg = _client.SendAsync(req);

				result.StatusCode = (int) msg.Result.StatusCode;

				var ret = msg.Result.Content.ReadAsStringAsync();

				result.Message = ret.Result;
			}
			catch (Exception ex)
			{
				result.Message = "[copy] Error: " + ex.Message;
			}

			return result;
		}

		/// <summary>
		/// 复制文件 (with 'force' param)
		/// </summary>
		/// <param name="srcBucket"></param>
		/// <param name="srcKey"></param>
		/// <param name="dstBucket"></param>
		/// <param name="dstKey"></param>
		/// <param name="force"></param>
		/// <returns></returns>
		public HttpResult Copy(string srcBucket, string srcKey, string dstBucket, string dstKey, bool force)
		{
			var result = new HttpResult();
			var copyUrl = Config.Zone.RsHost + CopyOp(srcBucket, srcKey, dstBucket, dstKey, force);
			var token = CreateManageToken(copyUrl);

			var req = new HttpRequestMessage(HttpMethod.Post, copyUrl);
			req.Headers.Add("Authorization", token);

			try
			{
				var msg = _client.SendAsync(req);

				result.StatusCode = (int) msg.Result.StatusCode;

				var ret = msg.Result.Content.ReadAsStringAsync();

				result.Message = ret.Result;
			}
			catch (Exception ex)
			{
				result.Message = "[copy] Error: " + ex.Message;
			}

			return result;
		}

		/// <summary>
		/// 移动文件
		/// </summary>
		/// <param name="srcBucket"></param>
		/// <param name="srcKey"></param>
		/// <param name="dstBucket"></param>
		/// <param name="dstKey"></param>
		/// <returns></returns>
		public HttpResult Move(string srcBucket, string srcKey, string dstBucket, string dstKey)
		{
			var result = new HttpResult();
			var moveUrl = Config.Zone.RsHost + MoveOp(srcBucket, srcKey, dstBucket, dstKey);
			var token = CreateManageToken(moveUrl);

			var req = new HttpRequestMessage(HttpMethod.Post, moveUrl);
			req.Headers.Add("Authorization", token);

			try
			{
				var msg = _client.SendAsync(req);

				result.StatusCode = (int) msg.Result.StatusCode;

				var ret = msg.Result.Content.ReadAsStringAsync();

				result.Message = ret.Result;
			}
			catch (Exception ex)
			{
				result.Message = "[move] Error: " + ex.Message;
			}

			return result;
		}

		/// <summary>
		/// 移动文件 (with 'force' param)
		/// </summary>
		/// <param name="srcBucket"></param>
		/// <param name="srcKey"></param>
		/// <param name="dstBucket"></param>
		/// <param name="dstKey"></param>
		/// <param name="force"></param>
		/// <returns></returns>
		public HttpResult Move(string srcBucket, string srcKey, string dstBucket, string dstKey, bool force)
		{
			var result = new HttpResult();
			var moveUrl = Config.Zone.RsHost + MoveOp(srcBucket, srcKey, dstBucket, dstKey, force);
			var token = CreateManageToken(moveUrl);

			var req = new HttpRequestMessage(HttpMethod.Post, moveUrl);
			req.Headers.Add("Authorization", token);

			try
			{
				var msg = _client.SendAsync(req);

				result.StatusCode = (int) msg.Result.StatusCode;

				var ret = msg.Result.Content.ReadAsStringAsync();

				result.Message = ret.Result;
			}
			catch (Exception ex)
			{
				result.Message = "[move] Error: " + ex.Message;
			}

			return result;
		}

		/// <summary>
		/// 修改文件MimeType
		/// </summary>
		/// <param name="bucket"></param>
		/// <param name="key"></param>
		/// <param name="mimeType"></param>
		/// <returns></returns>
		public HttpResult Chgm(string bucket, string key, string mimeType)
		{
			var result = new HttpResult();
			var chgmUrl = Config.Zone.RsHost + ChgmOp(bucket, key, mimeType);
			var token = CreateManageToken(chgmUrl);

			var req = new HttpRequestMessage(HttpMethod.Post, chgmUrl);
			req.Headers.Add("Authorization", token);

			try
			{
				var msg = _client.SendAsync(req);

				result.StatusCode = (int) msg.Result.StatusCode;

				var ret = msg.Result.Content.ReadAsStringAsync();

				result.Message = ret.Result;
			}
			catch (Exception ex)
			{
				result.Message = "[chgm] Error: " + ex.Message;
			}

			return result;
		}

		/// <summary>
		/// 批处理
		/// </summary>
		/// <param name="batchOps"></param>
		/// <returns></returns>
		public HttpResult Batch(string batchOps)
		{
			var result = new HttpResult();
			var batchUrl = Config.Zone.RsHost + "/batch";
			var content = Encoding.UTF8.GetBytes(batchOps);
			var token = CreateManageToken(batchUrl, content);

			var req = new HttpRequestMessage(HttpMethod.Post, batchUrl);
			req.Headers.Add("Authorization", token);
			req.Content = new ByteArrayContent(content);
			req.Content.Headers.Add("Content-Type", "application/x-www-form-urlencoded");

			try
			{
				var msg = _client.SendAsync(req);

				result.StatusCode = (int) msg.Result.StatusCode;

				var ret = msg.Result.Content.ReadAsStringAsync();

				result.Message = ret.Result;
			}
			catch (Exception ex)
			{
				result.Message = "[batch] Error: " + ex.Message;
			}

			return result;
		}

		/// <summary>
		/// 批处理
		/// </summary>
		/// <param name="ops"></param>
		/// <returns></returns>
		public HttpResult Batch(string[] ops)
		{
			var opsb = new StringBuilder();

			opsb.AppendFormat("op={0}", ops[0]);

			for (var i = 1; i < ops.Length; ++i)
			{
				opsb.AppendFormat("&op={0}", ops[i]);
			}

			return Batch(opsb.ToString());
		}

		/// <summary>
		/// 批处理-stat
		/// </summary>
		/// <param name="bucket"></param>
		/// <param name="keys"></param>
		/// <returns></returns>
		public StatResult[] BatchStat(string bucket, string[] keys)
		{
			var results = new StatResult[keys.Length];
			var ops = new string[keys.Length];

			for (var i = 0; i < keys.Length; ++i)
			{
				ops[i] = StatOp(bucket, keys[i]);

				results[i] = new StatResult();
			}

			var batchResult = Batch(ops);
			var infos = JsonConvert.DeserializeObject<List<BatchInfo>>(batchResult.Message);

			for (var i = 0; i < keys.Length; ++i)
			{
				results[i].StatusCode = infos[i].Code;
				results[i].StatInfo = JsonConvert.DeserializeObject<StatInfo>(infos[i].Data.ToString());
				results[i].Message = infos[i].Data.ToString();
			}

			return results;
		}

		/// <summary>
		/// 批处理 - delete
		/// </summary>
		/// <param name="bucket"></param>
		/// <param name="keys"></param>
		/// <returns></returns>
		public HttpResult[] BatchDelete(string bucket, string[] keys)
		{
			var results = new HttpResult[keys.Length];
			var ops = new string[keys.Length];

			for (var i = 0; i < keys.Length; ++i)
			{
				ops[i] = DeleteOp(bucket, keys[i]);
				results[i] = new HttpResult();
			}

			var deleteResult = Batch(ops);
			var infos = JsonConvert.DeserializeObject<List<BatchInfo>>(deleteResult.Message);

			for (var i = 0; i < keys.Length; ++i)
			{
				results[i].StatusCode = infos[i].Code;

				if (infos[i].Data != null)
				{
					results[i].Message = infos[i].Data.ToString();
				}
			}

			return results;
		}

		/// <summary>
		/// 抓取文件
		/// </summary>
		/// <param name="resUrl"></param>
		/// <param name="bucket"></param>
		/// <param name="key"></param>
		/// <returns></returns>
		public HttpResult Fetch(string resUrl, string bucket, string key)
		{
			var result = new HttpResult();
			var fetchUrl = Config.Zone.IovipHost + FetchOp(resUrl, bucket, key);
			var token = CreateManageToken(fetchUrl);
			var req = new HttpRequestMessage(HttpMethod.Post, fetchUrl);

			req.Headers.Add("Authorization", token);

			try
			{
				var msg = _client.SendAsync(req);

				result.StatusCode = (int) msg.Result.StatusCode;

				var ret = msg.Result.Content.ReadAsStringAsync();

				result.Message = ret.Result;
			}
			catch (Exception ex)
			{
				result.Message = "[fetch] Error: " + ex.Message;
			}

			return result;
		}

		/// <summary>
		/// 更新文件，适用于"镜像源站"设置的空间
		/// </summary>
		/// <param name="bucket"></param>
		/// <param name="key"></param>
		/// <returns></returns>
		public HttpResult Prefetch(string bucket, string key)
		{
			var result = new HttpResult();
			var prefetchUrl = Config.Zone.IovipHost + PrefetchOp(bucket, key);
			var token = CreateManageToken(prefetchUrl);

			var req = new HttpRequestMessage(HttpMethod.Post, prefetchUrl);
			req.Headers.Add("Authorization", token);

			try
			{
				var msg = _client.SendAsync(req);

				result.StatusCode = (int) msg.Result.StatusCode;

				var ret = msg.Result.Content.ReadAsStringAsync();

				result.Message = ret.Result;
			}
			catch (Exception ex)
			{
				result.Message = "[prefetch] Error: " + ex.Message;
			}

			return result;
		}

		/// <summary>
		/// 获取空间的域名
		/// </summary>
		/// <param name="bucket"></param>
		/// <returns></returns>
		public DomainsResult Domains(string bucket)
		{
			var result = new DomainsResult();
			var domainsUrl = Config.Zone.ApiHost + "/v6/domain/list";
			var body = string.Format("tbl={0}", bucket);
			var content = Encoding.UTF8.GetBytes(body);
			var token = CreateManageToken(domainsUrl, content);

			var req = new HttpRequestMessage(HttpMethod.Post, domainsUrl);
			req.Headers.Add("Authorization", token);
			req.Content = new ByteArrayContent(content);
			req.Content.Headers.Add("Content-Type", "application/x-www-form-urlencoded");

			try
			{
				var msg = _client.SendAsync(req);

				result.StatusCode = (int) msg.Result.StatusCode;

				var ret = msg.Result.Content.ReadAsStringAsync();

				result.Domains = JsonConvert.DeserializeObject<List<string>>(ret.Result);
			}
			catch (Exception ex)
			{
				result.Message = "[domains] Error: " + ex.Message;
			}

			return result;
		}

		/// <summary>
		/// 
		/// 获取空间文件列表 
		/// list(bucket, prefix, marker, limit, delimiter)
		/// 
		/// bucket:    目标空间名称
		/// 
		/// prefix:    返回指定文件名前缀的文件列表(prefix可设为null)
		/// 
		/// marker:    考虑到设置limit后返回的文件列表可能不全(需要重复执行listFiles操作)
		///            执行listFiles操作时使用marker标记来追加新的结果
		///            特别注意首次执行listFiles操作时marker为null
		///            
		/// limit:     每次返回结果所包含的文件总数限制(limit最大值1000，建议值100)
		/// 
		/// delimiter: 分隔符，比如-或者/等等，可以模拟作为目录结构(参考下述示例)
		///            假设指定空间中有2个文件 fakepath/1.txt fakepath/2.txt
		///            现设置分隔符delimiter = / 得到返回结果items =[]，commonPrefixes = [fakepath/]
		///            然后调整prefix = fakepath/ delimiter = null 得到所需结果items = [1.txt,2.txt]
		///            于是可以在本地先创建一个目录fakepath,然后在该目录下写入items中的文件
		///            
		/// </summary>
		public ListResult List(string bucket, string prefix, string marker, int limit, string delimiter)
		{
			var result = new ListResult();
			var sb = new StringBuilder("bucket=" + bucket);

			if (!string.IsNullOrEmpty(marker)) sb.Append("&marker=" + marker);
			if (!string.IsNullOrEmpty(prefix)) sb.Append("&prefix=" + prefix);
			if (!string.IsNullOrEmpty(delimiter)) sb.Append("&delimiter=" + delimiter);

			if ((limit > 1000) || (limit < 1)) sb.Append("&limit=1000");
			else sb.Append("&limit=" + limit);

			var listUrl = Config.Zone.RsfHost + "/list?" + sb;
			var token = CreateManageToken(listUrl);

			var req = new HttpRequestMessage(HttpMethod.Post, listUrl);
			req.Headers.Add("Authorization", token);

			try
			{
				var msg = _client.SendAsync(req);

				result.StatusCode = (int) msg.Result.StatusCode;

				var ret = msg.Result.Content.ReadAsStringAsync();

				result.ListInfo = JsonConvert.DeserializeObject<ListInfo>(ret.Result);
			}
			catch (Exception ex)
			{
				result.Message = "[list] Error: " + ex.Message;
			}

			return result;
		}

		/// <summary>
		/// 生成stat操作字符串
		/// </summary>
		/// <param name="bucket"></param>
		/// <param name="key"></param>
		/// <returns></returns>
		public string StatOp(string bucket, string key) { return string.Format("/stat/{0}", StringHelper.EncodedEntry(bucket, key)); }

		/// <summary>
		/// 生成delete操作字符串
		/// </summary>
		/// <param name="bucket"></param>
		/// <param name="key"></param>
		/// <returns></returns>
		public string DeleteOp(string bucket, string key) { return string.Format("/delete/{0}", StringHelper.EncodedEntry(bucket, key)); }

		/// <summary>
		/// 生成copy操作字符串
		/// </summary>
		/// <param name="srcBucket"></param>
		/// <param name="srcKey"></param>
		/// <param name="dstBucket"></param>
		/// <param name="dstKey"></param>
		/// <returns></returns>
		public string CopyOp(string srcBucket, string srcKey, string dstBucket, string dstKey)
		{
			return string.Format("/copy/{0}/{1}",
				StringHelper.EncodedEntry(srcBucket, srcKey),
				StringHelper.EncodedEntry(dstBucket, dstKey));
		}

		/// <summary>
		/// 生成copy(with 'force' param)操作字符串
		/// </summary>
		/// <param name="srcBucket"></param>
		/// <param name="srcKey"></param>
		/// <param name="dstBucket"></param>
		/// <param name="dstKey"></param>
		/// <param name="force"></param>
		/// <returns></returns>
		public string CopyOp(string srcBucket, string srcKey, string dstBucket, string dstKey, bool force)
		{
			var fx = force ? "force/true" : "force/false";

			return string.Format("/copy/{0}/{1}/{2}",
				StringHelper.EncodedEntry(srcBucket, srcKey),
				StringHelper.EncodedEntry(dstBucket, dstKey), fx);
		}

		/// <summary>
		/// 生成move操作字符串
		/// </summary>
		/// <param name="srcBucket"></param>
		/// <param name="srcKey"></param>
		/// <param name="destBucket"></param>
		/// <param name="destKey"></param>
		/// <returns></returns>
		public string MoveOp(string srcBucket, string srcKey, string destBucket, string destKey)
		{
			return string.Format("/move/{0}/{1}",
				StringHelper.EncodedEntry(srcBucket, srcKey),
				StringHelper.EncodedEntry(destBucket, destKey));
		}

		/// <summary>
		/// 生成copy(with 'force' param)操作字符串
		/// </summary>
		/// <param name="srcBucket"></param>
		/// <param name="srcKey"></param>
		/// <param name="destBucket"></param>
		/// <param name="destKey"></param>
		/// <param name="force"></param>
		/// <returns></returns>
		public string MoveOp(string srcBucket, string srcKey, string destBucket, string destKey, bool force)
		{
			var fx = force ? "force/true" : "force/false";

			return string.Format("/move/{0}/{1}/{2}",
				StringHelper.EncodedEntry(srcBucket, srcKey),
				StringHelper.EncodedEntry(destBucket, destKey), fx);
		}

		/// <summary>
		/// 生成chgm操作字符串
		/// </summary>
		/// <param name="bucket"></param>
		/// <param name="key"></param>
		/// <param name="mimeType"></param>
		/// <returns></returns>
		public string ChgmOp(string bucket, string key, string mimeType)
		{
			return string.Format("/chgm/{0}/mime/{1}",
				StringHelper.EncodedEntry(bucket, key),
				StringHelper.UrlSafeBase64Encode(mimeType));
		}

		/// <summary>
		/// 生成fetch操作字符串
		/// </summary>
		/// <param name="url"></param>
		/// <param name="bucket"></param>
		/// <param name="key"></param>
		/// <returns></returns>
		public string FetchOp(string url, string bucket, string key)
		{
			return string.Format("/fetch/{0}/to/{1}",
				StringHelper.UrlSafeBase64Encode(url),
				StringHelper.EncodedEntry(bucket, key));
		}

		/// <summary>
		/// 生成prefetch操作字符串
		/// </summary>
		/// <param name="bucket"></param>
		/// <param name="key"></param>
		/// <returns></returns>
		public string PrefetchOp(string bucket, string key) { return string.Format("/prefetch/{0}", StringHelper.EncodedEntry(bucket, key)); }
	}
}
