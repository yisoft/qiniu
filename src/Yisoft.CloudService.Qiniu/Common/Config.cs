﻿//      )                             *     
//   ( /(        *   )       (      (  `    
//   )\()) (   ` )  /( (     )\     )\))(   
//  ((_)\  )\   ( )(_)))\ ((((_)(  ((_)()\  
// __ ((_)((_) (_(_())((_) )\ _ )\ (_()((_) 
// \ \ / / (_) |_   _|| __|(_)_\(_)|  \/  | 
//  \ V /  | | _ | |  | _|  / _ \  | |\/| | 
//   |_|   |_|(_)|_|  |___|/_/ \_\ |_|  |_| 
// 
// This file is subject to the terms and conditions defined in
// file 'License.txt', which is part of this source code package.
// 
// Copyright © Yi.TEAM. All rights reserved.
// -------------------------------------------------------------------------------

namespace Yisoft.CloudService.Qiniu.Common
{
	/// <summary>
	/// 配置信息，主要包括Zone配置(另请参阅Zone模块)
	/// 目前已支持的机房包括：
	/// 华东(CN_East), 华北(CN_North), 华南(CN_South), 北美(US_North)
	/// 默认设置为华东机房(CN_East) 
	/// </summary>
	public class Config
	{
		// Fusion API
		public const string FUSION_API_HOST = "http://fusion.qiniuapi.com";

		// 空间所在的区域(Zone)
		public static Zone Zone = Zone.ZONE_CN_East();

		/// <summary>
		/// 根据Zone配置对应参数(RS_HOST,API_HOST等)
		/// </summary>
		/// <param name="zoneId">ZoneID</param>
		public static void SetZone(ZoneId zoneId)
		{
			switch (zoneId)
			{
				case ZoneId.CnEast:
					Zone = Zone.ZONE_CN_East();
					break;

				case ZoneId.CnNorth:
					Zone = Zone.ZONE_CN_North();
					break;

				case ZoneId.CnSouth:
					Zone = Zone.ZONE_CN_South();
					break;

				case ZoneId.UsNorth:
					Zone = Zone.ZONE_US_North();
					break;
			}
		}
	}
}
