﻿//      )                             *     
//   ( /(        *   )       (      (  `    
//   )\()) (   ` )  /( (     )\     )\))(   
//  ((_)\  )\   ( )(_)))\ ((((_)(  ((_)()\  
// __ ((_)((_) (_(_())((_) )\ _ )\ (_()((_) 
// \ \ / / (_) |_   _|| __|(_)_\(_)|  \/  | 
//  \ V /  | | _ | |  | _|  / _ \  | |\/| | 
//   |_|   |_|(_)|_|  |___|/_/ \_\ |_|  |_| 
// 
// This file is subject to the terms and conditions defined in
// file 'License.txt', which is part of this source code package.
// 
// Copyright © Yi.TEAM. All rights reserved.
// -------------------------------------------------------------------------------

using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using Yisoft.CloudService.Qiniu.Util;

namespace Yisoft.CloudService.Qiniu.Common
{
	/// <summary>
	/// 签名/加密
	/// </summary>
	public class Signature
	{
		private readonly Mac _mac;

		public Signature(Mac mac) { _mac = mac; }

		private string _EncodedSign(byte[] data)
		{
			var hmac = new HMACSHA1(Encoding.UTF8.GetBytes(_mac.SecretKey));
			var digest = hmac.ComputeHash(data);
			return StringHelper.UrlSafeBase64Encode(digest);
		}

		private string _EncodedSign(string str)
		{
			var data = Encoding.UTF8.GetBytes(str);

			return _EncodedSign(data);
		}

		public string Sign(byte[] data) { return string.Format("{0}:{1}", _mac.AccessKey, _EncodedSign(data)); }

		public string Sign(string str)
		{
			var data = Encoding.UTF8.GetBytes(str);
			return Sign(data);
		}

		public string SignWithData(byte[] data)
		{
			var sstr = StringHelper.UrlSafeBase64Encode(data);

			return string.Format("{0}:{1}:{2}", _mac.AccessKey, _EncodedSign(sstr), sstr);
		}

		public string SignWithData(string str)
		{
			var data = Encoding.UTF8.GetBytes(str);

			return SignWithData(data);
		}

		public string SignRequest(string url, byte[] body)
		{
			var u = new Uri(url);

			using (var hmac = new HMACSHA1(Encoding.UTF8.GetBytes(_mac.SecretKey)))
			{
				var pathAndQuery = u.PathAndQuery;
				var pathAndQueryBytes = Encoding.UTF8.GetBytes(pathAndQuery);

				using (var buffer = new MemoryStream())
				{
					buffer.Write(pathAndQueryBytes, 0, pathAndQueryBytes.Length);
					buffer.WriteByte((byte) '\n');

					if ((body != null) && (body.Length > 0))
					{
						buffer.Write(body, 0, body.Length);
					}

					var digest = hmac.ComputeHash(buffer.ToArray());
					var digestBase64 = StringHelper.UrlSafeBase64Encode(digest);

					return string.Format("{0}:{1}", _mac.AccessKey, digestBase64);
				}
			}
		}

		public string SignRequest(string url, string body)
		{
			var data = Encoding.UTF8.GetBytes(body);

			return SignRequest(url, data);
		}
	}
}
