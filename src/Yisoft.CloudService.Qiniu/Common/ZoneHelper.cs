﻿//      )                             *     
//   ( /(        *   )       (      (  `    
//   )\()) (   ` )  /( (     )\     )\))(   
//  ((_)\  )\   ( )(_)))\ ((((_)(  ((_)()\  
// __ ((_)((_) (_(_())((_) )\ _ )\ (_()((_) 
// \ \ / / (_) |_   _|| __|(_)_\(_)|  \/  | 
//  \ V /  | | _ | |  | _|  / _ \  | |\/| | 
//   |_|   |_|(_)|_|  |___|/_/ \_\ |_|  |_| 
// 
// This file is subject to the terms and conditions defined in
// file 'License.txt', which is part of this source code package.
// 
// Copyright © Yi.TEAM. All rights reserved.
// -------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Net.Http;
using Newtonsoft.Json;

namespace Yisoft.CloudService.Qiniu.Common
{
	public class ZoneHelper
	{
		/// <summary>
		/// Zone
		/// 不同区域upHost分别唯一，据此确定对应的Zone
		/// </summary>
		private static readonly Dictionary<string, ZoneId> _ZoneDict = new Dictionary<string, ZoneId>
		{
			{"http://up.qiniu.com", ZoneId.CnEast},
			{"http://up-z1.qiniu.com", ZoneId.CnNorth},
			{"http://up-z2.qiniu.com", ZoneId.CnSouth},
			{"http://up-na0.qiniu.com", ZoneId.UsNorth}
		};

		/// <summary>
		/// 从uc.qbox.me查询得到回复后，解析出upHost,然后根据upHost确定Zone
		/// </summary>
		/// <param name="accessKey">AK</param>
		/// <param name="bucket">Bucket</param>
		public static ZoneId QueryZone(string accessKey, string bucket)
		{
			ZoneId zoneId;

			// HTTP/GET https://uc.qbox.me/v1/query?ak=(AK)&bucket=(Bucket)
			// 该请求的返回数据参见后面的 QueryResponse 结构
			// 根据response消息提取出upHost
			var queryUrl = string.Format("https://uc.qbox.me/v1/query?ak={0}&bucket={1}", accessKey, bucket);

			try
			{
				var client = new HttpClient();
				var msg = client.GetStringAsync(queryUrl);
				var qr = JsonConvert.DeserializeObject<QueryResponse>(msg.Result);
				var upHost = qr.Http.Up[0];

				zoneId = _ZoneDict[upHost];
			}
			catch (Exception ex)
			{
				throw new Exception("[ConfigZone] Error: " + ex.Message);
			}

			return zoneId;
		}

		#region UC_QUERY_RESPONSE

		// 从uc.qbox.me返回的消息，使用Json解析
		// 以下是一个response示例
		// {
		//     "ttl" : 86400,
		//     "http" : {
		//         "up" : [
		//                     "http://up.qiniu.com",
		//                     "http://upload.qiniu.com",
		//                     "-H up.qiniu.com http://183.136.139.16"
		//                 ],
		//        "io" : [
		//                      "http://iovip.qbox.me"
		//                ]
		//             },
		//     "https" : {
		//          "io" : [
		//                     "https://iovip.qbox.me"
		//                  ],
		//         "up" : [
		//                     "https://up.qbox.me"
		//                  ]
		//                  }
		// }

		/// <summary>
		/// 从uc.qbox.me返回的消息
		/// </summary>
		internal class QueryResponse
		{
			public string Ttl { get; set; }

			public HttpBulk Http { get; set; }

			public HttpBulk Https { get; set; }
		}

		/// <summary>
		/// HttpBulk作为QueryResponse的成员
		/// 包含uploadHost和iovip等
		/// </summary>
		internal class HttpBulk
		{
			public string[] Up { get; set; }

			public string[] Io { get; set; }
		}

		#endregion UC_QUERY_RESPONSE  
	}
}
