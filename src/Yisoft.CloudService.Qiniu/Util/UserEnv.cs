﻿//      )                             *     
//   ( /(        *   )       (      (  `    
//   )\()) (   ` )  /( (     )\     )\))(   
//  ((_)\  )\   ( )(_)))\ ((((_)(  ((_)()\  
// __ ((_)((_) (_(_())((_) )\ _ )\ (_()((_) 
// \ \ / / (_) |_   _|| __|(_)_\(_)|  \/  | 
//  \ V /  | | _ | |  | _|  / _ \  | |\/| | 
//   |_|   |_|(_)|_|  |___|/_/ \_\ |_|  |_| 
// 
// This file is subject to the terms and conditions defined in
// file 'License.txt', which is part of this source code package.
// 
// Copyright © Yi.TEAM. All rights reserved.
// -------------------------------------------------------------------------------

using System;

namespace Yisoft.CloudService.Qiniu.Util
{
	internal class UserEnv
	{
		/// <summary>
		/// 获取home路径
		/// </summary>
		/// <returns></returns>
		public static string HomeFolder()
		{
			// Windows下Home目录 = %HOMEDRIVE% + %HOMEPATH%
			var homeFolder = Environment.GetEnvironmentVariable("HOMEDRIVE") + Environment.GetEnvironmentVariable("HOMEPATH");

			if (string.IsNullOrEmpty(homeFolder))
			{
				// OSX/Ubuntu下Home目录 = $HOME
				homeFolder = Environment.GetEnvironmentVariable("HOME");
			}

			if (string.IsNullOrEmpty(homeFolder))
			{
				// 如果获取失败，就设置为./
				homeFolder = "./";
			}

			return homeFolder;
		}
	}
}
