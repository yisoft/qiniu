﻿//      )                             *     
//   ( /(        *   )       (      (  `    
//   )\()) (   ` )  /( (     )\     )\))(   
//  ((_)\  )\   ( )(_)))\ ((((_)(  ((_)()\  
// __ ((_)((_) (_(_())((_) )\ _ )\ (_()((_) 
// \ \ / / (_) |_   _|| __|(_)_\(_)|  \/  | 
//  \ V /  | | _ | |  | _|  / _ \  | |\/| | 
//   |_|   |_|(_)|_|  |___|/_/ \_\ |_|  |_| 
// 
// This file is subject to the terms and conditions defined in
// file 'License.txt', which is part of this source code package.
// 
// Copyright © Yi.TEAM. All rights reserved.
// -------------------------------------------------------------------------------

using System.IO;

namespace Yisoft.CloudService.Qiniu.Util
{
	/// <summary>
	/// CRC32计算器
	/// </summary>
	internal class CRC32
	{
		public const uint IEEE = 0xedb88320;
		private readonly uint[] _table;
		private uint _value;

		public CRC32()
		{
			_value = 0;
			_table = _MakeTable(IEEE);
		}

		public void Write(byte[] p, int offset, int count) { _value = Update(_value, _table, p, offset, count); }

		public uint Sum32() { return _value; }

		private static uint[] _MakeTable(uint poly)
		{
			var table = new uint[256];

			for (var i = 0; i < 256; i++)
			{
				var crc = (uint) i;

				for (var j = 0; j < 8; j++)
				{
					if ((crc & 1) == 1) crc = (crc >> 1) ^ poly;
					else crc >>= 1;
				}

				table[i] = crc;
			}

			return table;
		}

		public static uint Update(uint crc, uint[] table, byte[] p, int offset, int count)
		{
			crc = ~crc;

			for (var i = 0; i < count; i++)
			{
				crc = table[((byte) crc) ^ p[offset + i]] ^ (crc >> 8);
			}

			return ~crc;
		}

		/// <summary>
		/// 计算字节数据的crc32值
		/// </summary>
		/// <param name="data">二进制数据</param>
		/// <returns>crc32值</returns>
		public static uint CheckSumBytes(byte[] data)
		{
			var crc = new CRC32();

			crc.Write(data, 0, data.Length);

			return crc.Sum32();
		}

		public static uint CheckSumSlice(byte[] data, int offset, int count)
		{
			var crc = new CRC32();

			crc.Write(data, offset, count);

			return crc.Sum32();
		}

		/// <summary>
		/// 计算沙盒文件的crc32值
		/// </summary>
		/// <param name="filePath">沙盒文件全路径</param>
		/// <returns>crc32值</returns>
		public static uint CheckSumFile(string filePath)
		{
			const int buffer_len = 32 * 1024;
			var crc = new CRC32();

			using (var fs = new FileStream(filePath, FileMode.Open, FileAccess.Read))
			{
				var buffer = new byte[buffer_len];

				while (true)
				{
					var n = fs.Read(buffer, 0, buffer_len);

					if (n == 0) break;

					crc.Write(buffer, 0, n);
				}
			}

			return crc.Sum32();
		}
	}
}
