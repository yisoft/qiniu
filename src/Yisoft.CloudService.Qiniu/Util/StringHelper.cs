﻿//      )                             *     
//   ( /(        *   )       (      (  `    
//   )\()) (   ` )  /( (     )\     )\))(   
//  ((_)\  )\   ( )(_)))\ ((((_)(  ((_)()\  
// __ ((_)((_) (_(_())((_) )\ _ )\ (_()((_) 
// \ \ / / (_) |_   _|| __|(_)_\(_)|  \/  | 
//  \ V /  | | _ | |  | _|  / _ \  | |\/| | 
//   |_|   |_|(_)|_|  |___|/_/ \_\ |_|  |_| 
// 
// This file is subject to the terms and conditions defined in
// file 'License.txt', which is part of this source code package.
// 
// Copyright © Yi.TEAM. All rights reserved.
// -------------------------------------------------------------------------------

using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using Newtonsoft.Json;

namespace Yisoft.CloudService.Qiniu.Util
{
	/// <summary>
	/// 字符串处理工具
	/// </summary>
	internal class StringHelper
	{
		/// <summary>
		/// 字符串连接
		/// </summary>
		/// <param name="array">字符串数组</param>
		/// <param name="sep">连接符</param>
		/// <returns>连接后字符串</returns>
		public static string Join(string[] array, string sep)
		{
			if ((array == null) || (sep == null)) return null;

			var joined = new StringBuilder();
			var arrayLength = array.Length;

			for (var i = 0; i < arrayLength; i++)
			{
				joined.Append(array[i]);

				if (i < arrayLength - 1)
				{
					joined.Append(sep);
				}
			}

			return joined.ToString();
		}

		/// <summary>
		/// 以json格式连接字符串
		/// </summary>
		/// <param name="array">字符串数组</param>
		/// <returns>连接后字符串</returns>
		public static string JsonJoin(string[] array)
		{
			if (array == null) return null;

			var joined = new StringBuilder();
			var arrayLength = array.Length;

			for (var i = 0; i < arrayLength; i++)
			{
				joined.Append("\"").Append(array[i]).Append("\"");

				if (i < arrayLength - 1)
				{
					joined.Append(",");
				}
			}

			return joined.ToString();
		}

		/// <summary>
		/// 获取字符串Url安全Base64编码值
		/// </summary>
		/// <param name="from">源字符串</param>
		/// <returns>已编码字符串</returns>
		public static string UrlSafeBase64Encode(string from) { return UrlSafeBase64Encode(Encoding.UTF8.GetBytes(from)); }

		public static string UrlSafeBase64Encode(byte[] from) { return Convert.ToBase64String(from).Replace('+', '-').Replace('/', '_'); }

		/// <summary>
		/// 解码Url安全的Base64编码值
		/// </summary>
		/// <param name="from">编码字符串</param>
		/// <returns>已解码字符串</returns>
		public static byte[] UrlsafeBase64Decode(string from) { return Convert.FromBase64String(from.Replace('-', '+').Replace('_', '/')); }

		public static T JsonDecode<T>(string jsonData) { return JsonConvert.DeserializeObject<T>(jsonData); }

		public static string EncodedEntry(string bucket, string key) { return key == null ? UrlSafeBase64Encode(bucket) : UrlSafeBase64Encode(bucket + ":" + key); }

		public static byte[] CalcSHA1(byte[] data)
		{
			using (var sha1 = SHA1.Create())
			{
				return sha1.ComputeHash(data);
			}
		}

		public static string UrlEncode(string from) { return Uri.EscapeDataString(from); }

		public static string UrlValuesEncode(Dictionary<string, string> values)
		{
			var urlValuesBuilder = new StringBuilder();

			foreach (var kvp in values)
			{
				urlValuesBuilder.AppendFormat("{0}={1}&", Uri.EscapeDataString(kvp.Key), Uri.EscapeDataString(kvp.Value));
			}

			var encodedStr = urlValuesBuilder.ToString();

			return encodedStr.Substring(0, encodedStr.Length - 1);
		}
	}
}
