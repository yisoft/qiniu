﻿//      )                             *     
//   ( /(        *   )       (      (  `    
//   )\()) (   ` )  /( (     )\     )\))(   
//  ((_)\  )\   ( )(_)))\ ((((_)(  ((_)()\  
// __ ((_)((_) (_(_())((_) )\ _ )\ (_()((_) 
// \ \ / / (_) |_   _|| __|(_)_\(_)|  \/  | 
//  \ V /  | | _ | |  | _|  / _ \  | |\/| | 
//   |_|   |_|(_)|_|  |___|/_/ \_\ |_|  |_| 
// 
// This file is subject to the terms and conditions defined in
// file 'License.txt', which is part of this source code package.
// 
// Copyright © Yi.TEAM. All rights reserved.
// -------------------------------------------------------------------------------

using System;
using System.IO;

namespace Yisoft.CloudService.Qiniu.Util
{
	public class QETag
	{
		// 块大小(固定为4MB)
		private const int _BLOCK_SIZE = 4 * 1024 * 1024;

		// 计算时以20B为单位
		private static readonly int _BlockSHA1Size = 20;

		public static string CalcHash(string filePath)
		{
			var qetag = "";

			try
			{
				using (var stream = new FileStream(filePath, FileMode.Open, FileAccess.Read))
				{
					var fileLength = stream.Length;
					var buffer = new byte[_BLOCK_SIZE];
					var finalBuffer = new byte[_BlockSHA1Size + 1];

					if (fileLength <= _BLOCK_SIZE)
					{
						var readByteCount = stream.Read(buffer, 0, _BLOCK_SIZE);

						var readBuffer = new byte[readByteCount];
						Array.Copy(buffer, readBuffer, readByteCount);

						finalBuffer[0] = 0x16;
						var sha1Buffer = StringHelper.CalcSHA1(readBuffer);
						Array.Copy(sha1Buffer, 0, finalBuffer, 1, sha1Buffer.Length);
					}
					else
					{
						var blockCount = (fileLength % _BLOCK_SIZE == 0) ? (fileLength / _BLOCK_SIZE) : (fileLength / _BLOCK_SIZE + 1);
						var sha1AllBuffer = new byte[_BlockSHA1Size * blockCount];

						for (var i = 0; i < blockCount; i++)
						{
							var readByteCount = stream.Read(buffer, 0, _BLOCK_SIZE);
							var readBuffer = new byte[readByteCount];
							Array.Copy(buffer, readBuffer, readByteCount);

							var sha1Buffer = StringHelper.CalcSHA1(readBuffer);
							Array.Copy(sha1Buffer, 0, sha1AllBuffer, i * _BlockSHA1Size, sha1Buffer.Length);
						}

						finalBuffer[0] = 0x96;
						var sha1AllBufferSha1 = StringHelper.CalcSHA1(sha1AllBuffer);
						Array.Copy(sha1AllBufferSha1, 0, finalBuffer, 1, sha1AllBufferSha1.Length);
					}
					qetag = StringHelper.UrlSafeBase64Encode(finalBuffer);
				}
			}
			catch (Exception)
			{
				// ignored
			}

			return qetag;
		}
	}
}
