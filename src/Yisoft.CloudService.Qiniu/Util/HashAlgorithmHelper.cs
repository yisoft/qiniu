﻿//      )                             *     
//   ( /(        *   )       (      (  `    
//   )\()) (   ` )  /( (     )\     )\))(   
//  ((_)\  )\   ( )(_)))\ ((((_)(  ((_)()\  
// __ ((_)((_) (_(_())((_) )\ _ )\ (_()((_) 
// \ \ / / (_) |_   _|| __|(_)_\(_)|  \/  | 
//  \ V /  | | _ | |  | _|  / _ \  | |\/| | 
//   |_|   |_|(_)|_|  |___|/_/ \_\ |_|  |_| 
// 
// This file is subject to the terms and conditions defined in
// file 'License.txt', which is part of this source code package.
// 
// Copyright © Yi.TEAM. All rights reserved.
// -------------------------------------------------------------------------------

using System;
using System.Security.Cryptography;
using System.Text;

namespace Yisoft.CloudService.Qiniu.Util
{
	/// <summary>
	/// 提供哈希算法常用操作的封装。
	/// </summary>
	internal static class HashAlgorithmHelper
	{
		private static string _ComputeHash(HashAlgorithm hashAlgorithm, string input, bool lowerCase = true, Encoding encoding = null)
		{
			if (encoding == null) encoding = Encoding.UTF8;

			var buffer = hashAlgorithm.ComputeHash(encoding.GetBytes(input));
			var result = BitConverter.ToString(buffer).Replace("-", string.Empty);

			return lowerCase ? result.ToLower() : result;
		}

		public static string MD5(string input, bool lowerCase = true, bool shortValue = false, Encoding encoding = null)
		{
			using (var hashAlgorithm = System.Security.Cryptography.MD5.Create())
			{
				var result = _ComputeHash(hashAlgorithm, input, lowerCase, encoding);

				return shortValue ? result.Substring(8, 16) : result;
			}
		}
	}
}
