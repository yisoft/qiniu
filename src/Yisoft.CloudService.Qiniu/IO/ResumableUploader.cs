﻿//      )                             *     
//   ( /(        *   )       (      (  `    
//   )\()) (   ` )  /( (     )\     )\))(   
//  ((_)\  )\   ( )(_)))\ ((((_)(  ((_)()\  
// __ ((_)((_) (_(_())((_) )\ _ )\ (_()((_) 
// \ \ / / (_) |_   _|| __|(_)_\(_)|  \/  | 
//  \ V /  | | _ | |  | _|  / _ \  | |\/| | 
//   |_|   |_|(_)|_|  |___|/_/ \_\ |_|  |_| 
// 
// This file is subject to the terms and conditions defined in
// file 'License.txt', which is part of this source code package.
// 
// Copyright © Yi.TEAM. All rights reserved.
// -------------------------------------------------------------------------------

using System;
using System.IO;
using System.Net.Http;
using System.Text;
using Newtonsoft.Json;
using Yisoft.CloudService.Qiniu.Common;
using Yisoft.CloudService.Qiniu.Extensions;
using Yisoft.CloudService.Qiniu.Http;
using Yisoft.CloudService.Qiniu.IO.Model;
using Yisoft.CloudService.Qiniu.Util;

namespace Yisoft.CloudService.Qiniu.IO
{
	/// <summary>
	/// 分片上传/断点续上传，适合于以下情形(2)(3)
	/// (1)网络较好并且待上传的文件体积较小时使用简单上传
	/// (2)文件较大或者网络状况不理想时请使用分片上传
	/// (3)文件较大上传需要花费较长时间，建议使用断点续上传
	/// </summary>
	public class ResumableUploader
	{
		//分片上传块的大小，固定为4M，不可修改
		private const int _BLOCK_SIZE = 4 * 1024 * 1024;

		//分片上传切片大小(要求:一个块能够恰好被分割为若干片)，可设置
		private readonly int _chunkSize = 2 * 1024 * 1024;

		// HTTP CLIENT
		private readonly HttpClient _client;

		public ResumableUploader() { _client = new HttpClient(); }

		/// <summary>
		/// 分片上传/断点续上传
		/// 使用默认记录文件(recordFile)和默认进度处理(progressHandler)
		/// </summary>
		/// <param name="localFile">本地待上传的文件名</param>
		/// <param name="saveKey">要保存的文件名称</param>
		/// <param name="token">上传凭证</param>
		/// <returns></returns>
		public HttpResult UploadFile(string localFile, string saveKey, string token)
		{
			var ruFile = "QiniuRU_" + (localFile + saveKey).MD5();
			var recordFile = Path.Combine(UserEnv.HomeFolder(), ruFile);

			UploadProgressHandler uppHandler = DefaultUploadProgressHandler;

			return UploadFile(localFile, saveKey, token, recordFile, uppHandler);
		}

		/// <summary>
		/// 分片上传/断点续上传
		/// 使用默认进度处理(progressHandler)
		/// </summary>
		/// <param name="localFile">本地待上传的文件名</param>
		/// <param name="saveKey">要保存的文件名称</param>
		/// <param name="token">上传凭证</param>
		/// <param name="recordFile">记录文件(记录断点信息)</param>
		/// <returns></returns>
		public HttpResult UploadFile(string localFile, string saveKey, string token, string recordFile)
		{
			UploadProgressHandler uppHandler = DefaultUploadProgressHandler;

			return UploadFile(localFile, saveKey, token, recordFile, uppHandler);
		}

		/// <summary>
		/// 分片上传/断点续上传
		/// </summary>
		/// <param name="localFile">本地待上传的文件名</param>
		/// <param name="saveKey">要保存的文件名称</param>
		/// <param name="token">上传凭证</param>
		/// <param name="recordFile">记录文件(记录断点信息)</param>
		/// <param name="uppHandler">上传进度处理</param>
		/// <returns></returns>
		public HttpResult UploadFile(string localFile, string saveKey, string token, string recordFile, UploadProgressHandler uppHandler)
		{
			var result = new HttpResult();

			FileStream fs = null;

			try
			{
				fs = new FileStream(localFile, FileMode.Open);

				long chunkSize = _chunkSize;
				long blockSize = _BLOCK_SIZE;
				var fileSize = fs.Length;
				var chunkBuffer = new byte[chunkSize];
				var blockCount = (int) ((fileSize + blockSize - 1) / blockSize);
				var resumeInfo = ResumeHelper.Load(recordFile);

				if (resumeInfo == null)
				{
					resumeInfo = new ResumeInfo
					{
						FileSize = fileSize,
						BlockIndex = 0,
						BlockCount = blockCount,
						Contexts = new string[blockCount]
					};

					ResumeHelper.Save(resumeInfo, recordFile);
				}

				var index = resumeInfo.BlockIndex;
				var offset = index * blockSize;
				var leftBytes = fileSize - offset;

				fs.Seek(offset, SeekOrigin.Begin);

				while (leftBytes > 0)
				{
					blockSize = leftBytes < _BLOCK_SIZE ? leftBytes : _BLOCK_SIZE;
					chunkSize = leftBytes < _chunkSize ? leftBytes : _chunkSize;

					fs.Read(chunkBuffer, 0, (int) chunkSize);

					var hr = _Mkblk(chunkBuffer, blockSize, chunkSize, token);

					if (hr.StatusCode != 200)
					{
						result.StatusCode = hr.StatusCode;

						throw new Exception(hr.Message);
					}

					var rc = JsonConvert.DeserializeObject<ResumeContext>(hr.Message);
					var context = rc.Ctx;

					offset += chunkSize;
					leftBytes -= chunkSize;

					uppHandler(offset, fileSize);

					if (leftBytes > 0)
					{
						var blockLeft = blockSize - chunkSize;
						var blockOffset = chunkSize;

						while (blockLeft > 0)
						{
							chunkSize = blockLeft < _chunkSize ? blockLeft : _chunkSize;

							fs.Read(chunkBuffer, 0, (int) chunkSize);

							hr = _Bput(chunkBuffer, blockOffset, chunkSize, context, token);

							if (hr.StatusCode != 200)
							{
								result.StatusCode = hr.StatusCode;

								throw new Exception(hr.Message);
							}

							rc = JsonConvert.DeserializeObject<ResumeContext>(hr.Message);
							context = rc.Ctx;

							offset += chunkSize;
							leftBytes -= chunkSize;
							blockOffset += chunkSize;
							blockLeft -= chunkSize;

							uppHandler(offset, fileSize);
						}
					}

					resumeInfo.BlockIndex = index;
					resumeInfo.Contexts[index] = context;

					ResumeHelper.Save(resumeInfo, recordFile);

					++index;
				}

				var mkfileResult = _Mkfile(localFile, fileSize, saveKey, "application/octet-stream", resumeInfo.Contexts, token);

				if (mkfileResult.StatusCode != 200)
				{
					result.StatusCode = mkfileResult.StatusCode;

					throw new Exception(mkfileResult.Message);
				}

				File.Delete(recordFile);

				result.StatusCode = 200;
				result.Message = string.Format("[ResumableUpload] Uploaded: \"{0}\" ==> \"{1}\"", localFile, saveKey);
			}
			catch (Exception ex)
			{
				result.Message = "[ResumableUpload] Error: " + ex.Message;
			}
			finally
			{
				fs?.Dispose();
			}

			return result;
		}

		/// <summary>
		/// 默认的进度处理函数
		/// </summary>
		/// <param name="uploadedBytes">已上传的字节数</param>
		/// <param name="totalBytes">文件总字节数</param>
		public void DefaultUploadProgressHandler(long uploadedBytes, long totalBytes)
		{
			if (uploadedBytes < totalBytes)
			{
				Console.WriteLine("[ResumableUpload] Progress: {0,7:0.000}%", 100.0 * uploadedBytes / totalBytes);
			}
			else
			{
				Console.WriteLine("[ResumableUpload] Progress: finished");
			}
		}

		/// <summary>
		/// 创建文件
		/// </summary>
		private HttpResult _Mkfile(string fileName, long size, string saveKey, string mimeType, string[] contexts, string token)
		{
			var result = new HttpResult();

			var fnameStr = string.Format("/fname/{0}", StringHelper.UrlSafeBase64Encode(fileName));
			var mimeTypeStr = string.Format("/mimeType/{0}", StringHelper.UrlSafeBase64Encode(mimeType));
			var keyStr = string.Format("/key/{0}", StringHelper.UrlSafeBase64Encode(saveKey));

			var url = $"{Config.Zone.UpHost}/mkfile/{size}{mimeTypeStr}{fnameStr}{keyStr}";
			var body = StringHelper.Join(contexts, ",");

			var req = new HttpRequestMessage(HttpMethod.Post, url);
			req.Headers.Add("Authorization", string.Format("UpToken {0}", token));
			req.Content = new ByteArrayContent(Encoding.UTF8.GetBytes(body));
			req.Content.Headers.Add("Content-Type", "application/x-www-form-urlencoded");

			try
			{
				var msg = _client.SendAsync(req);

				result.StatusCode = (int) msg.Result.StatusCode;

				var ret = msg.Result.Content.ReadAsStringAsync();

				result.Message = ret.Result;
			}
			catch (Exception ex)
			{
				result.Message = "[mkfile] Error:" + ex.Message;
			}

			return result;
		}

		/// <summary>
		/// 创建块(携带首片数据)
		/// </summary>
		private HttpResult _Mkblk(byte[] chunkBuffer, long blockSize, long chunkSize, string token)
		{
			var result = new HttpResult();
			var url = string.Format("{0}/mkblk/{1}", Config.Zone.UpHost, blockSize);

			try
			{
				var req = new HttpRequestMessage(HttpMethod.Post, url);
				req.Headers.Add("Authorization", string.Format("UpToken {0}", token));
				req.Content = new ByteArrayContent(chunkBuffer, 0, (int) chunkSize);
				req.Content.Headers.Add("Content-Type", "application/octet-stream");

				var msg = _client.SendAsync(req);

				result.StatusCode = (int) msg.Result.StatusCode;

				var ret = msg.Result.Content.ReadAsStringAsync();

				result.Message = ret.Result;
			}
			catch (Exception ex)
			{
				result.Message = "[mkblk] Error:" + ex.Message;
			}

			return result;
		}

		/// <summary>
		/// 上传数据片
		/// </summary>
		private HttpResult _Bput(byte[] chunkBuffer, long offset, long chunkSize, string context, string token)
		{
			var result = new HttpResult();
			var url = string.Format("{0}/bput/{1}/{2}", Config.Zone.UpHost, context, offset);

			try
			{
				var req = new HttpRequestMessage(HttpMethod.Post, url);
				req.Headers.Add("Authorization", string.Format("UpToken {0}", token));
				req.Content = new ByteArrayContent(chunkBuffer, 0, (int) chunkSize);
				req.Content.Headers.Add("Content-Type", "application/octet-stream");

				var msg = _client.SendAsync(req);

				result.StatusCode = (int) msg.Result.StatusCode;

				var ret = msg.Result.Content.ReadAsStringAsync();

				result.Message = ret.Result;
			}
			catch (Exception ex)
			{
				result.Message = "[bput] Error:" + ex.Message;
			}

			return result;
		}
	}
}
