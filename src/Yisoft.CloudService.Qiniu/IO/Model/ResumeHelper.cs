﻿//      )                             *     
//   ( /(        *   )       (      (  `    
//   )\()) (   ` )  /( (     )\     )\))(   
//  ((_)\  )\   ( )(_)))\ ((((_)(  ((_)()\  
// __ ((_)((_) (_(_())((_) )\ _ )\ (_()((_) 
// \ \ / / (_) |_   _|| __|(_)_\(_)|  \/  | 
//  \ V /  | | _ | |  | _|  / _ \  | |\/| | 
//   |_|   |_|(_)|_|  |___|/_/ \_\ |_|  |_| 
// 
// This file is subject to the terms and conditions defined in
// file 'License.txt', which is part of this source code package.
// 
// Copyright © Yi.TEAM. All rights reserved.
// -------------------------------------------------------------------------------

using System;
using System.IO;
using Newtonsoft.Json;
using Yisoft.CloudService.Qiniu.Util;

namespace Yisoft.CloudService.Qiniu.IO.Model
{
	/// <summary>
	/// 断点续上传辅助函数Load/Save
	/// </summary>
	public class ResumeHelper
	{
		public static ResumeInfo Load(string recordFile)
		{
			ResumeInfo resumeInfo;

			try
			{
				using (var fs = new FileStream(recordFile, FileMode.Open))
				{
					using (var sr = new StreamReader(fs))
					{
						var jsonStr = sr.ReadToEnd();

						resumeInfo = JsonConvert.DeserializeObject<ResumeInfo>(jsonStr);
					}
				}
			}
			catch (Exception)
			{
				resumeInfo = null;
			}

			return resumeInfo;
		}

		public static void Save(ResumeInfo resumeInfo, string recordFile)
		{
			var jsonStr =
				$"{{\"fileSize\":{resumeInfo.FileSize}, \"blockIndex\":{resumeInfo.BlockIndex}, \"blockCount\":{resumeInfo.BlockCount}, \"contexts\":[{StringHelper.JsonJoin(resumeInfo.Contexts)}]}}";

			using (var fs = new FileStream(recordFile, FileMode.Create))
			{
				using (var sw = new StreamWriter(fs))
				{
					sw.Write(jsonStr);
				}
			}
		}
	}
}
