﻿//      )                             *     
//   ( /(        *   )       (      (  `    
//   )\()) (   ` )  /( (     )\     )\))(   
//  ((_)\  )\   ( )(_)))\ ((((_)(  ((_)()\  
// __ ((_)((_) (_(_())((_) )\ _ )\ (_()((_) 
// \ \ / / (_) |_   _|| __|(_)_\(_)|  \/  | 
//  \ V /  | | _ | |  | _|  / _ \  | |\/| | 
//   |_|   |_|(_)|_|  |___|/_/ \_\ |_|  |_| 
// 
// This file is subject to the terms and conditions defined in
// file 'License.txt', which is part of this source code package.
// 
// Copyright © Yi.TEAM. All rights reserved.
// -------------------------------------------------------------------------------

using Newtonsoft.Json;

namespace Yisoft.CloudService.Qiniu.IO.Model
{
	/// <summary>
	/// 分片上传的上下文信息
	/// </summary>
	[JsonObject(MemberSerialization.OptIn)]
	public class ResumeContext
	{
		[JsonProperty("ctx")]
		public string Ctx { get; set; }

		[JsonProperty("checksum")]
		public string Checksum { get; set; }

		[JsonProperty("crc32")]
		public long Crc32 { get; set; }

		[JsonProperty("offset")]
		public long Offset { get; set; }

		[JsonProperty("host")]
		public string Host { get; set; }

		[JsonProperty("expired_at")]
		public long ExpiredAt { get; set; }
	}
}
