﻿//      )                             *     
//   ( /(        *   )       (      (  `    
//   )\()) (   ` )  /( (     )\     )\))(   
//  ((_)\  )\   ( )(_)))\ ((((_)(  ((_)()\  
// __ ((_)((_) (_(_())((_) )\ _ )\ (_()((_) 
// \ \ / / (_) |_   _|| __|(_)_\(_)|  \/  | 
//  \ V /  | | _ | |  | _|  / _ \  | |\/| | 
//   |_|   |_|(_)|_|  |___|/_/ \_\ |_|  |_| 
// 
// This file is subject to the terms and conditions defined in
// file 'License.txt', which is part of this source code package.
// 
// Copyright © Yi.TEAM. All rights reserved.
// -------------------------------------------------------------------------------

using System;
using Newtonsoft.Json;

namespace Yisoft.CloudService.Qiniu.IO.Model
{
	/// <summary>
	/// 上传策略
	/// 参阅http://developer.qiniu.com/article/developer/security/put-policy.html
	/// </summary>
	[JsonObject(MemberSerialization.OptIn)]
	public class PutPolicy
	{
		private TimeSpan _expires;

		[JsonProperty("scope")]
		public string Scope { set; get; }

		[JsonProperty("deadline")]
		public int Deadline { set; get; }

		[JsonProperty("insertOnly")]
		public int? InsertOnly { set; get; }

		[JsonProperty("saveKey")]
		public string SaveKey { set; get; }

		[JsonProperty("endUser")]
		public string EndUser { set; get; }

		[JsonProperty("returnUrl")]
		public string ReturnUrl { set; get; }

		[JsonProperty("returnBody")]
		public string ReturnBody { set; get; }

		[JsonProperty("callbackUrl")]
		public string CallbackUrl { set; get; }

		[JsonProperty("callbackBody")]
		public string CallbackBody { set; get; }

		[JsonProperty("callbackBodyType")]
		public string CallbackBodyType { set; get; }

		[JsonProperty("callbackHost")]
		public string CallbackHost { set; get; }

		[JsonProperty("callbackFetchKey")]
		public int? CallbackFetchKey { set; get; }

		[JsonProperty("persistentOps")]
		public string PersistentOps { set; get; }

		[JsonProperty("persistentNotifyUrl")]
		public string PersistentNotifyUrl { set; get; }

		[JsonProperty("persistentPipeline")]
		public string PersistentPipeline { set; get; }

		[JsonProperty("fsizeLimit")]
		public int? FsizeLimit { set; get; }

		[JsonProperty("detectMime")]
		public int? DetectMime { set; get; }

		[JsonProperty("mimeLimit")]
		public string MimeLimit { set; get; }

		[JsonProperty("deleteAfterDays")]
		public int? DeleteAfterDays { set; get; }

		[JsonIgnore]
		public TimeSpan Expires
		{
			get { return _expires; }
			set
			{
				_expires = value;

				var ts = DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1, 0, 0, 0));

				Deadline = (int) (ts.TotalSeconds + _expires.TotalSeconds);
			}
		}
	}
}
