﻿//      )                             *     
//   ( /(        *   )       (      (  `    
//   )\()) (   ` )  /( (     )\     )\))(   
//  ((_)\  )\   ( )(_)))\ ((((_)(  ((_)()\  
// __ ((_)((_) (_(_())((_) )\ _ )\ (_()((_) 
// \ \ / / (_) |_   _|| __|(_)_\(_)|  \/  | 
//  \ V /  | | _ | |  | _|  / _ \  | |\/| | 
//   |_|   |_|(_)|_|  |___|/_/ \_\ |_|  |_| 
// 
// This file is subject to the terms and conditions defined in
// file 'License.txt', which is part of this source code package.
// 
// Copyright © Yi.TEAM. All rights reserved.
// -------------------------------------------------------------------------------

using Newtonsoft.Json;
using Yisoft.CloudService.Qiniu.Common;
using Yisoft.CloudService.Qiniu.Http;
using Yisoft.CloudService.Qiniu.IO.Model;

namespace Yisoft.CloudService.Qiniu.IO
{
	/// <summary>
	/// 上传管理器，根据文件大小以及阈值设置自动选择合适的上传方式，支持以下(1)(2)(3)
	/// (1)网络较好并且待上传的文件体积较小时使用简单上传
	/// (2)文件较大或者网络状况不理想时请使用分片上传
	/// (3)文件较大上传需要花费较长时间，建议使用断点续上传
	/// </summary>
	public class UploadManager
	{
		private readonly Signature _signature;

		public UploadManager(Mac mac) { _signature = new Signature(mac); }

		/// <summary>
		/// 生成上传凭证
		/// 参阅 http://developer.qiniu.com/article/developer/security/upload-token.html
		/// </summary>
		/// <param name="putPolicy"></param>
		/// <returns></returns>
		public string CreateUploadToken(PutPolicy putPolicy)
		{
			var data = JsonConvert.SerializeObject(putPolicy);

			return _signature.SignWithData(data);
		}

		/// <summary>
		/// 上传文件
		/// </summary>
		/// <param name="localFile">本地待上传的文件名</param>
		/// <param name="saveKey">要保存的文件名称</param>
		/// <param name="putPolicy">上传策略</param>
		/// <returns></returns>
		public HttpResult UploadFile(string localFile, string saveKey, PutPolicy putPolicy)
		{
			var token = CreateUploadToken(putPolicy);

			return UploadFile(localFile, saveKey, token);
		}

		/// <summary>
		/// 上传文件
		/// </summary>
		/// <param name="localFile">本地待上传的文件名</param>
		/// <param name="saveKey">要保存的文件名称</param>
		/// <param name="token">上传凭证</param>
		/// <returns></returns>
		public HttpResult UploadFile(string localFile, string saveKey, string token)
		{
			var ru = new ResumableUploader();

			return ru.UploadFile(localFile, saveKey, token);
		}
	}
}
