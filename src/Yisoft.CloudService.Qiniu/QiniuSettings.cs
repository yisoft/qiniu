﻿//      )                             *     
//   ( /(        *   )       (      (  `    
//   )\()) (   ` )  /( (     )\     )\))(   
//  ((_)\  )\   ( )(_)))\ ((((_)(  ((_)()\  
// __ ((_)((_) (_(_())((_) )\ _ )\ (_()((_) 
// \ \ / / (_) |_   _|| __|(_)_\(_)|  \/  | 
//  \ V /  | | _ | |  | _|  / _ \  | |\/| | 
//   |_|   |_|(_)|_|  |___|/_/ \_\ |_|  |_| 
// 
// This file is subject to the terms and conditions defined in
// file 'License.txt', which is part of this source code package.
// 
// Copyright © Yi.TEAM. All rights reserved.
// -------------------------------------------------------------------------------

namespace Yisoft.CloudService.Qiniu
{
	public class QiniuSettings
	{
		/// <summary>
		/// SDK名称
		/// </summary>
		public const string ALIAS = "Qiniu.AspNetCore.CloudStorage";

		/// <summary>
		/// SDK版本号
		/// </summary>
		public const string VERSION = "1.0.0";

		/// <summary>
		/// SDK模块
		/// </summary>
		public static string[] Modules = {"IO", "RS", "RSF", "Util", "Common", "Http"};

		public string Accesskey { get; set; }

		public string SecretKey { get; set; }

		public string DefaultBucket { get; set; }

		public string DefaultPrefix { get; set; }

		public string DefaultDomainName { get; set; }
	}
}
