﻿//      )                             *     
//   ( /(        *   )       (      (  `    
//   )\()) (   ` )  /( (     )\     )\))(   
//  ((_)\  )\   ( )(_)))\ ((((_)(  ((_)()\  
// __ ((_)((_) (_(_())((_) )\ _ )\ (_()((_) 
// \ \ / / (_) |_   _|| __|(_)_\(_)|  \/  | 
//  \ V /  | | _ | |  | _|  / _ \  | |\/| | 
//   |_|   |_|(_)|_|  |___|/_/ \_\ |_|  |_| 
// 
// This file is subject to the terms and conditions defined in
// file 'License.txt', which is part of this source code package.
// 
// Copyright © Yi.TEAM. All rights reserved.
// -------------------------------------------------------------------------------

namespace Yisoft.CloudService.Qiniu.Http
{
	/// <summary>
	/// (HTTP请求的)返回消息
	/// </summary>
	public class HttpResult
	{
		public HttpResult()
		{
			StatusCode = 0;
			Message = "";
		}

		/// <summary>
		/// 状态码 (200表示OK)
		/// </summary>
		public int StatusCode { get; set; }

		/// <summary>
		/// 消息(或错误)文本
		/// </summary>
		public string Message { get; set; }
	}
}
