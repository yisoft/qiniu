﻿//      )                             *     
//   ( /(        *   )       (      (  `    
//   )\()) (   ` )  /( (     )\     )\))(   
//  ((_)\  )\   ( )(_)))\ ((((_)(  ((_)()\  
// __ ((_)((_) (_(_())((_) )\ _ )\ (_()((_) 
// \ \ / / (_) |_   _|| __|(_)_\(_)|  \/  | 
//  \ V /  | | _ | |  | _|  / _ \  | |\/| | 
//   |_|   |_|(_)|_|  |___|/_/ \_\ |_|  |_| 
// 
// This file is subject to the terms and conditions defined in
// file 'License.txt', which is part of this source code package.
// 
// Copyright © Yi.TEAM. All rights reserved.
// -------------------------------------------------------------------------------

using System;
using Yisoft.CloudService.Qiniu.Extensions;

namespace Yisoft.CloudService.Qiniu.Http
{
	public class HttpHelper
	{
		/// <summary>
		/// 客户端标识
		/// </summary>
		/// <returns></returns>
		public string GetUserAgent() { return string.Format("{0}/{1} ({2})", QiniuSettings.ALIAS, QiniuSettings.VERSION, Environment.MachineName); }

		/// <summary>
		/// 多部分表单数据(multi-part form-data)的分界(boundary)标识
		/// </summary>
		/// <returns></returns>
		public string CreateFormDataBoundary()
		{
			var now = DateTime.UtcNow.Ticks.ToString();

			return string.Format("-------{0}Boundary{1}", QiniuSettings.ALIAS, now.MD5());
		}
	}
}
